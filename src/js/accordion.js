function initiateAccordions() {
    var accordions = document.querySelectorAll('.accordion');
    for (var i = 0; i < accordions.length; i++) {
        var accItems = accordions[i].childNodes;
        accItems.forEach(function (item, i, arr) {
            item.addEventListener('click', function () {
                if (!this.classList.contains('accordion__item--opened')) {
                    this.classList.add('accordion__item--opened');
                } else {
                    this.classList.remove('accordion__item--opened');
                }
            });
        });
    }
}

initiateAccordions();
